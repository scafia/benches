'''
File: smc100.py
Project: stages
Created Date: Friday March 4th 2022
Author: Ronan (ronan.lashermes@inria.fr)
-----
Last Modified: Friday, 4th March 2022 10:23:15 am
Modified By: Ronan (ronan.lashermes@inria.fr>)
-----
Copyright (c) 2022 INRIA
'''

import warnings

import traceback
from helpers.uart import Uart
# import serial
from pint import Quantity, UnitRegistry
from time import sleep
from enum import Enum

def remove_prefix(text, prefix):
    return text[text.startswith(prefix) and len(prefix):]

class SMC100State(Enum):
    NOT_REFERENCED = 1
    CONFIGURATION = 2
    HOMING = 3
    MOVING = 4
    READY = 5
    DISABLE = 6
    JOGGING = 7

class SMC100:

    BAUD_RATE = 57600

    def name(self) -> str:
        return "SMC100 " + str(self.axis_id)

    def __init__(self, axis_id: int, uart: Uart):
        self.ureg = UnitRegistry()
        self.precision = Quantity(1, self.ureg.micrometer)
        self.axis_id = axis_id
        self.uart = uart
        
        
    def test(self) -> bool:
        self.send_command("ID?")
        ans = self.uart.read_until(b"\r\n")
        if self.uart.debug:
            print(ans)
        
        return ((str(self.axis_id) + "ID").encode('utf-8') in ans)

    # [cmd] without line termination nor axis
    # example send_command("OR") will send "1OR\r\n"
    def send_command(self, cmd: str):
        self.uart.send_str(str(self.axis_id) + cmd + "\r\n")
        if self.uart.debug:
            print("-> " + str(self.axis_id) + cmd)

    

    def send_request(self, cmd: str):
        self.send_command(cmd)
        ans = self.uart.read_until(b"\r\n").strip()
        if self.uart.debug:
            print("<- " + str(ans))
        prefix = (str(self.axis_id) + cmd).encode('utf-8')
        if ans.startswith(prefix):
            ans = ans[len(prefix):]
        return ans

    def getState(self) -> SMC100State:
        ans = self.send_request("TS")
        state = ans[4:]
        
        stateDict = {
            b"0A": SMC100State.NOT_REFERENCED,
            b"0B": SMC100State.NOT_REFERENCED,
            b"0C": SMC100State.NOT_REFERENCED,
            b"0D": SMC100State.NOT_REFERENCED,
            b"0E": SMC100State.NOT_REFERENCED,
            b"0F": SMC100State.NOT_REFERENCED,
            b"10": SMC100State.NOT_REFERENCED,
            b"11": SMC100State.NOT_REFERENCED,

            b"14": SMC100State.CONFIGURATION,

            b"1E": SMC100State.HOMING,
            b"1F": SMC100State.HOMING,

            b"28": SMC100State.MOVING,

            b"32": SMC100State.READY,
            b"33": SMC100State.READY,
            b"34": SMC100State.READY,
            b"35": SMC100State.READY,

            b"3C": SMC100State.DISABLE,
            b"3D": SMC100State.DISABLE,
            b"3E": SMC100State.DISABLE,

            b"46": SMC100State.JOGGING,
            b"47": SMC100State.JOGGING,
        }

        return stateDict[state]

    def needHoming(self) -> bool:
        return self.getState() == SMC100State.NOT_REFERENCED

    def homing(self, user_confirmed: bool = False):
        if self.getState() == SMC100State.NOT_REFERENCED:
            if user_confirmed == False:
                confirmation = input("Stage is about to perform HOMING operation: make sure that enough space is available.\n\rConfim (yes/*) ? ")
                if confirmation == "yes":
                    user_confirmed = True

            if user_confirmed == True:
                self.send_command("OR")
                sleep(0.5)
                self.waitForReady()
            else:
                print("Homing is cancelled: no user confirmation.")

    def reset(self):
        self.send_command("RS")

    def move(self, position:Quantity, blocking=True):
        posmm = position.to(self.ureg.mm)
        self.send_command("PA" + str(posmm.magnitude))

        sleep(0.1)
        if blocking:
            self.waitForMoveStop()

    def moveRelative(self, position:Quantity, blocking=True):
        posmm = position.to(self.ureg.mm)
        self.send_command("PR" + str(posmm.magnitude))

        sleep(0.1)
        if blocking:
            self.waitForMoveStop()

    def waitForMoveStop(self):
        while self.getState() == SMC100State.MOVING:
                sleep(0.5)
                
        finalState = self.getState()
        if finalState != SMC100State.READY:
            if finalState == SMC100State.NOT_REFERENCED:
                raise Exception("HOMING is required before first move.")
            else:
                raise Exception("Move failed: " + str(finalState))

    def waitForReady(self):
        while self.getState() != SMC100State.READY:
                sleep(0.5)


    def getPos(self) -> Quantity: 
        ans = float(self.send_request("TP"))
        return Quantity(ans, self.ureg.mm).to_compact()