'''
File: xyz_stage.py
Project: stages
Created Date: Friday March 4th 2022
Author: Ronan (ronan.lashermes@inria.fr)
-----
Last Modified: Tuesday, 15th March 2022 3:35:55 pm
Modified By: Ronan (ronan.lashermes@inria.fr>)
-----
Copyright (c) 2022 INRIA
'''

from __future__ import annotations
from decimal import DivisionByZero
from stages.smc100 import SMC100
from helpers.uart import Uart
from pint import Quantity, UnitRegistry
from time import sleep
from shapely.geometry import MultiPoint, Point, Polygon
import jsonpickle


class AreaOfInterest:

    def __init__(self, rect: Polygon, plane: tuple) -> None:
        self.rect = rect
        self.plane = plane

    def focus(self, focus: Polygon) -> AreaOfInterest:
        return AreaOfInterest(self.rect.intersection(focus), self.plane)

    def save(self, filepath):
        frozen = jsonpickle.encode(self)
        file = open(filepath, "w")
        file.write(frozen)
        file.close()

    def load(filepath) -> 'AreaOfInterest':
        with open(filepath, "r") as file:
            frozen = file.read()
            return jsonpickle.decode(frozen)


class XYZStage:


    def __init__(self, serial_port: str, timeout=1, debug=False) -> None:
        self.uart = Uart(serial_port, 57600, xonxoff=True, timeout=timeout, debug=debug)
        self.ureg = UnitRegistry()
        self.upDist = Quantity(1, self.ureg.mm)
        self.precision = Quantity(1, self.ureg.micrometer)

        self.x = SMC100(1, self.uart)
        self.y = SMC100(2, self.uart)
        self.z = SMC100(3, self.uart)

    def close(self):
        self.uart.close()

    def test(self) -> bool:
        return self.x.test() & self.y.test() & self.z.test()

    def zero(self):
        self.z.move(Quantity(0, self.ureg.mm), False)
        self.x.move(Quantity(0, self.ureg.mm), False)
        self.y.move(Quantity(0, self.ureg.mm), False)

        sleep(0.5)
        self.waitForMoveStop()

    def homing(self, user_confirmed: bool = False):
        self.x.homing(user_confirmed)
        self.y.homing(user_confirmed)
        self.z.homing(user_confirmed)

    def needHoming(self):
        return self.x.needHoming() or self.y.needHoming() or self.z.needHoming()

    def moveXY(self, xPos: Quantity, yPos: Quantity, up=True):

        zPos = self.z.getPos()
        if up:
            self.up()

        self.x.move(xPos, False)
        self.y.move(yPos, False)

        sleep(0.5)
        self.waitForMoveStop()

        if up:
            self.z.move(zPos)

    def moveXYZ_dimless(self, pointMM: tuple, up=True):
        xPos = Quantity(pointMM[0], self.ureg.mm)
        yPos = Quantity(pointMM[1], self.ureg.mm)
        zPos = Quantity(pointMM[2], self.ureg.mm)
        self.moveXYZ(xPos, yPos, zPos, up)


    def moveXYZ(self, xPos: Quantity, yPos: Quantity, zPos: Quantity, up=True):
        if up == True:
            self.z.move(zPos - self.upDist)

        self.x.move(xPos, False)
        self.y.move(yPos, False)
        if up == False:
            self.y.move(zPos, False)
        sleep(0.5)
        self.waitForMoveStop()
        
        if up == True:
            self.z.move(zPos)


    def waitForMoveStop(self):
        self.x.waitForMoveStop()
        self.y.waitForMoveStop()
        self.z.waitForMoveStop()

    def up(self):
        self.z.moveRelative(-self.upDist)

    def down(self):
        self.z.moveRelative(self.upDist)

    def __pointsToRectangle(self, points):
        dimless_points = list(map(self.__toDimensionlessMM, points))
        all_points = MultiPoint(dimless_points)
        rec = all_points.minimum_rotated_rectangle
        return rec

    def __frange(start: float, stop: float, step: float):
        current = start

        if (start < stop and step < 0.0) or (stop < start and step > 0.0):
            raise Exception("Infinite range is not supported.")

        while current < stop:
            yield current
            current += step

    def __crossProduct3D(a, b):
        c = [a[1]*b[2] - a[2]*b[1],
            a[2]*b[0] - a[0]*b[2],
            a[0]*b[1] - a[1]*b[0]]
        return c

    def __vectorFromPoints(p1, p2):
        vx = p2[0] - p1[0]
        vy = p2[1] - p1[1]
        vz = p2[2] - p1[2]
        return (vx, vy, vz)

    def __toDimensionlessMM(self, point):
        return (point[0].to(self.ureg.mm).magnitude, point[1].to(self.ureg.mm).magnitude, point[2].to(self.ureg.mm).magnitude)

    def getSurfaceIterator(self, area: AreaOfInterest, step: Quantity):
        # assume rect coordinates in ureg.mm
        x,y = area.rect.exterior.xy
        minX = min(x)
        maxX = max(x)
        minY = min(y)
        maxY = max(y)

        step_dimless = step.to(self.ureg.mm).magnitude

        grid = []
        for xPos in XYZStage.__frange(minX, maxX, step_dimless):
            for yPos in XYZStage.__frange(minY, maxY, step_dimless):
                p = Point(xPos, yPos)
                if p.within(area.rect):
                    grid.append((xPos, yPos, XYZStage.__getZfromPoint2D((xPos, yPos), area.plane)))

        return grid

    def __getZfromPoint2D(point2D, plane) -> float:
        if plane[2] == 0.0:
            raise DivisionByZero()
        z = - (plane[0] * point2D[0] + plane[1] * point2D[1] + plane[3]) / plane[2]
        return z

    def __planeFrom3Points(self, p1, p2, p3):

        p1 = self.__toDimensionlessMM(p1)
        p2 = self.__toDimensionlessMM(p2)
        p3 = self.__toDimensionlessMM(p3)

        v1 = XYZStage.__vectorFromPoints(p1, p2)
        v2 = XYZStage.__vectorFromPoints(p1, p3)

        n = XYZStage.__crossProduct3D(v1,v2)
        d = - (n[0]*p1[0] + n[1]*p1[1] + n[2]*p1[2])
        n.append(d)
        return n

    def __averagePlanes(ns):
        av = [0.0, 0.0, 0.0, 0.0]
        for n in ns:
            av[0] += n[0]
            av[1] += n[1]
            av[2] += n[2]
            av[3] += n[3]

        l = len(ns)
        av[0] /= l
        av[1] /= l
        av[2] /= l
        av[3] /= l
        return av

    def __planeFromPoints(self, points):
        if len(points) < 3:
            raise Exception("Not enough points to generate a plane (<3).")

        ns = []
        for i in range(len(points)-2):
            n = self.__planeFrom3Points(points[i], points[i+1], points[i+2])
            ns.append(n)

        return XYZStage.__averagePlanes(ns)

    def computeAreaOfInterest(self, points) -> AreaOfInterest:
        rect = self.__pointsToRectangle(points)
        plane = self.__planeFromPoints(points)

        return AreaOfInterest(rect, plane)