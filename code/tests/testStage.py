
from helpers.uart import Uart
from stages.smc100 import SMC100
from stages.xyz_stage import XYZStage, AreaOfInterest
from pint import Quantity, UnitRegistry
from time import sleep

if __name__ == "__main__":

    ureg = UnitRegistry()
    #uart = Uart("/dev/ttyUSB0", 57600, xonxoff=True, debug=False)
    #smc = SMC100(2, uart)
    #print(smc.test())
    #smc.homing()

    #smc.move(Quantity(-10, ureg.mm))
    #print(smc.getPos())
    #smc.move(Quantity(10, ureg.mm))
    #print(smc.getPos())
    
    stage = XYZStage("/dev/ttyUSB0")
    area = AreaOfInterest.load("area")
    
    stage.homing()

    points = stage.getSurfaceIterator(area, Quantity(1, ureg.mm))
    print(str(len(points)) + " points to iterate.")

    for point in points:
        stage.moveXYZ_dimless(point)
        sleep(1)
