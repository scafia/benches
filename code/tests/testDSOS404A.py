'''
File: test.py
Project: oscilloscopes
Created Date: Monday October 5th 2020
Author: Ronan (ronan.lashermes@inria.fr)
-----
Last Modified: Wednesday, 14th October 2020 11:49:19 am
Modified By: Ronan (ronan.lashermes@inria.fr>)
-----
Copyright (c) 2020 INRIA
'''

from oscilloscopes.dsos404a import Dsos404a
from helpers.ip_connection import IpConnection

from pint import Quantity

if __name__ == "__main__":

    # conf_file = open("config_test2.txt", "rb")
    # conf = conf_file.read()
    # conf_file.close()

    # print(conf)

    osc = Dsos404a(IpConnection())

    # conf = osc.read_configuration()
    osc.reset_configuration()
    # osc.load_configuration(conf)

    # text_file = open("config_test2.txt", "wb")
    # text_file.write(conf)
    # text_file.close()

    print("Name: ", osc.name())

    c1scale = osc.get_vertical_div("CHAN1")
    print("CHAN1 SCALE: ", c1scale)

    osc.set_sampling_count(100000)
    nb_segmenst = osc.set_segmented(95000)
    print("Number of segments: ", nb_segmenst)

    osc.set_realtime()

    osc.stop()
    print("State 1: ", osc.get_run_state())

    osc.run()
    print("State 2: ", osc.get_run_state())

    osc.single()
    print("State 3: ", osc.get_run_state())

    # osc.set_enabled_channels(["CHAN2", "CHAN3"])
    # # enabled_channels = osc.get_enabled_channels()
    # # print(enabled_channels)

    # osc.set_vertical_position("CHAN2", Quantity("500mV"))
    # off2 = osc.get_vertical_position("CHAN2")
    # print("CHAN2 offset: ", off2)

    
    # osc.set_horizontal_div(Quantity("200ns"))
    # hdiv = osc.get_horizontal_div()
    # osc.set_horizontal_div(hdiv)
    # print("HDIV: ", hdiv)

    # # trace = osc.get_trace("CHAN2")
    # # print(trace)

    # sr = osc.get_sampling_rate()
    # print("Sampling rate: ", sr)

    # sc = osc.get_sampling_count()
    # print("Sampling count: ", sc)