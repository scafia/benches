'''
File: test.py
Project: oscilloscopes
Created Date: Monday October 5th 2020
Author: Ronan (ronan.lashermes@inria.fr)
-----
Last Modified: Wednesday, 10th November 2021 4:38:03 pm
Modified By: Ronan (ronan.lashermes@inria.fr>)
-----
Copyright (c) 2020 INRIA
'''

from helpers.openocd import OpenOCD


if __name__ == "__main__":
    # requires a STM32VLDISCOVERY board plugged
    
    # OpenOCD.autonomous_reset(["tests/stm32vldiscovery.cfg"])    

    ocd = OpenOCD(["tests/stm32vldiscovery.cfg"])
    ocd.reset()

    ocd.read_memory_range(0x08000210, 16)