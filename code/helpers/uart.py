'''
File: uart.py
Project: helpers
Created Date: Friday March 4th 2022
Author: Ronan (ronan.lashermes@inria.fr)
-----
Last Modified: Friday, 4th March 2022 9:30:08 am
Modified By: Ronan (ronan.lashermes@inria.fr>)
-----
Copyright (c) 2022 INRIA
'''

import serial
import time

class Uart:

    def __init__(self, serial_port, baudrate, xonxoff=False, timeout=3, debug=False):
        self.serial_handle = serial.Serial(serial_port, baudrate, xonxoff=xonxoff, timeout=timeout)
        self.debug = debug
        self.timeout = timeout

    # RAW SERIAL

    def send_bytes(self, bytes):
        self.serial_handle.write(bytes)

    def send_str(self, str):
        self.serial_handle.write(str.encode('utf-8'))

    def read_bytes(self, count):
        return self.serial_handle.read(count)

    def read_until(self, end_bytes):
        buffer = bytearray()

        start = time.time()
        
        while buffer.endswith(end_bytes) == False:
            now = time.time()
            if now-start > self.timeout:
                break
            buffer.extend(self.read_bytes(1))

        return bytes(buffer)