'''
File: ip_connection.py
Project: helpers
Created Date: Monday October 5th 2020
Author: Ronan (ronan.lashermes@inria.fr)
-----
Last Modified: Monday, 5th October 2020 11:16:42 am
Modified By: Ronan (ronan.lashermes@inria.fr>)
-----
Copyright (c) 2020 INRIA
'''

class IpConnection:

    def __init__(self, address=None, port=None, to=None):
        if address is None:
            self.address = "131.254.13.161"
        else:
            self.address = address
            
        if port is None:
            self.port = 5025
        else:
            self.port = port

        if to is None:
            self.to = 100
        else:
            self.to = to