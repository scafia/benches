'''
File: openocd.py
Project: helpers
Created Date: Friday November 5th 2021
Author: Ronan (ronan.lashermes@inria.fr)
-----
Last Modified: Tuesday, 18th October 2022 3:13:01 pm
Modified By: Ronan (ronan.lashermes@inria.fr>)
-----
Copyright (c) 2021 INRIA
'''

import shlex, subprocess
import warnings
import socket
import time
from os.path import exists
from typing import List

class OpenOCD:

    def __init__(self, configuration_files: List[str], debug=False):
        self.config_files = configuration_files
        self.debug = debug

        # test files existance
        for cfile in self.config_files:
            if exists(cfile) == False:
                raise FileNotFoundError(cfile + ' does not exits')

        self.__launch_server()

        self.error_flags_description = {
            "UNDEFINSTR":       "the processor has attempted to execute an undefined instruction",
            "INVSTATE":         "the processor has attempted to execute an instruction that makes illegal use of the EPSR",
            "INVPC":            "the processor has attempted an illegal load of EXC_RETURN to the PC, as a result of an invalid context, or an invalid EXC_RETURN value",
            "NOCP":             "the processor has attempted to access a coprocessor",
            "UNALIGNED":        "the processor has made an unaligned memory access",
            "DIVBYZERO":        "the processor has executed an SDIV or UDIV instruction with a divisor of 0",
            "IBUSERR":          "instruction bus error",
            "PRECISERR":        "a data bus error has occurred, and the PC value stacked for the exception return points to the instruction that caused the fault",
            "IMPRECISERR":      "a data bus error has occurred, but the return address in the stack frame is not related to the instruction that caused the error",
            "UNSTKERR":         "unstack for an exception return has caused one or more BusFaults",
            "STKERR":           "stacking for an exception entry has caused one or more BusFaults",
            "BFARVALID":        "BFAR holds a valid fault address",
            "IACCVIOL":         "the processor attempted an instruction fetch from a location that does not permit execution",
            "DACCVIOL":         "the processor attempted a load or store at a location that does not permit the operation",
            "MUNSTKERR":        "unstack for an exception return has caused one or more access violations",
            "MSTKERR":          "stacking for an exception entry has caused one or more access violations",
            "MMARVALID":        "MMAR holds a valid fault address",
            "FORCED":           "forced HardFault",
            "VECTTBLE":         "BusFault on vector table read"
        }

    def __del__(self):
        if hasattr(self, 'ocd_server'):
            self.ocd_server.kill()


    def __launch_server(self):
        cmd = ['openocd']
        # set config files
        for cfile in self.config_files:
            cmd.append('-f')
            cmd.append(cfile)

        self.ocd_server = subprocess.Popen(cmd, stderr=subprocess.DEVNULL)
        # wait some time for the server to start.
        
        time.sleep(1)
        # by default, OpenOCD opens a server at localhost:4444
        
        self.session = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.session.settimeout(3.0)
        self.session.connect(('localhost', 4444))
        
        self.__read_until(b"> ")
    

    def __send_str(self, s:str):
        """Send a string to the OpenOCD server."""
        if isinstance(s, str):
            bs = s.encode("utf8")
            self.session.sendall(bs)

    def __read_until(self, p:bytes):
        """Read data from the OpenOCD server until the byte pattern is found."""
        b = bytearray()
        pattern_size = len(p)
        while True:
            b.extend(self.session.recv(1))
            last_pattern = b[-pattern_size:]
            if last_pattern == p:
                return b

    def __read_line(self):
        """Read one line from the OpenOCD server."""
        return self.__read_until(b"\n")

    def autonomous_reset(configuration_files):
        """Trigger an OpenOCD reset with a shell command, without using the OCD server."""

        for cfile in configuration_files:
            if exists(cfile) == False:
                raise FileNotFoundError(cfile + ' does not exits')

        cmd = 'openocd'
        # set config files
        for cfile in configuration_files:
            cmd += ' -f ' + cfile

        # reset
        cmd += ' -c "init\nreset run\n exit\n"'
        cmd_args = shlex.split(cmd)


        subprocess.run(cmd_args, capture_output=False, stderr=subprocess.DEVNULL)

    def reset(self):
        self.__send_str("reset run\n")
        self.__read_until(b"> ")
        self.halted = False

    def halt(self):
        self.__send_str("halt\n")
        self.__read_until(b"halt\r\n")
        self.__read_until(b"> ")
        self.halted = True
        

    def resume(self):
        self.__send_str("resume\n")
        self.__read_until(b"resume\r\n")
        self.__read_until(b"> ")
        self.halted = False

    def read_memory_range(self, base_address:int, byte_count:int, auto_resume:bool = False):
        self.halt()
        self.__send_str("mdb " + hex(base_address) + " " + str(byte_count) + "\n")
        r = self.__read_until(b"> ")

        if auto_resume:
            self.resume()

        lines = r.split(b"\r\n")
        barr = bytearray()
        for line in lines[1:-1]:
            address_split = line.split(b":")
            if len(address_split) > 1:
                byte_vals = address_split[1].decode(encoding="utf-8")
                byte_vals = byte_vals.replace(" ", "")
                barr.extend(bytes.fromhex(byte_vals))
        return barr

    def write_memory_u32(self, base_address: int, value: int, auto_resume:bool = False):
        self.halt()
        print("Writing", "mww " + hex(base_address) + " " + hex(value))
        self.__send_str("mww " + hex(base_address) + " " + hex(value) + "\n")       
        self.__read_until(b"> ") 

        if auto_resume:
            self.resume()

    def read_all_registers(self, auto_resume:bool = False):
        self.halt()
        self.__send_str("reg\n")
        r = self.__read_until(b"> ")

        if auto_resume:
            self.resume()

        dic = dict()

        lines = r.split(b"\r\n")
        for line in lines[1:-1]:
            reg_split = line.split(b":")
            if len(reg_split) > 1:
                rname = OpenOCD.__extract_reg_name(reg_split[0].decode(encoding="utf-8"))
                rvalue = OpenOCD.__extract_reg_value(reg_split[1].decode(encoding="utf-8"))
                dic[rname] = rvalue

        return dic

    # Return (UFSR, BFSR, MMFSR)
    def read_ConfigurableFaultStatusRegister(self, auto_resume:bool = False):
        self.halt()

        b = self.read_memory_range(0xE000ED28, 4)

        if auto_resume:
            self.resume()

        ufsr = b[0:2]
        bfsr = b[2]
        mmfsr = b[3]

        return (int.from_bytes(ufsr, byteorder='little'), bfsr, mmfsr)

    # Read error registers and extract error flags
    def investigate_fault(self, write_report_stdout:bool =False):
        (ufsr, bfsr, mmfsr) = self.read_ConfigurableFaultStatusRegister()
        hfsr = self.read_HardFaultStatusRegister()

        error_flags = []
        error_flags.extend(OpenOCD.extract_bfsr_flags(bfsr))
        error_flags.extend(OpenOCD.extract_mmfsr_flags(mmfsr))
        error_flags.extend(OpenOCD.extract_ufsr_flags(ufsr))
        error_flags.extend(OpenOCD.extract_hfsr_flags(hfsr))

        for flag in error_flags:
            desc = self.error_flags_description.get(flag, "")
            if write_report_stdout:
                print("%s: %s" %(flag, desc))

        if "BFARVALID" in error_flags:
            bus_FA = self.read_BusFaultAddressRegister()
            print("Bus Fault Address =", hex(bus_FA))

        if "MMARVALID" in error_flags:
            mm_FA = self.read_MemManageFaultAddressRegister()
            print("Mem Manage Fault Address =", hex(mm_FA))

        return error_flags

    def extract_ufsr_flags(ufsr: int):
        flags = []

        if ufsr & (1<<9) != 0:
            flags.append("DIVBYZERO")
        if ufsr & (1<<8) != 0:
            flags.append("UNALIGNED")
        if ufsr & (1<<3) != 0:
            flags.append("NOCP")
        if ufsr & (1<<2) != 0:
            flags.append("INVPC")
        if ufsr & (1<<1) != 0:
            flags.append("INVSTATE")
        if ufsr & 1 != 0:
            flags.append("UNDEFINSTR")

        return flags

    def extract_mmfsr_flags(mmfsr: int):
        flags = []

        if mmfsr & (1<<7) != 0:
            flags.append("MMARVALID")
        if mmfsr & (1<<4) != 0:
            flags.append("MSTKERR")
        if mmfsr & (1<<3) != 0:
            flags.append("MUNSTKERR")
        if mmfsr & (1<<1) != 0:
            flags.append("DACCVIOL")
        if mmfsr & 1 != 0:
            flags.append("IACCVIOL")

        return flags

    def extract_bfsr_flags(bfsr: int):
        flags = []

        if bfsr & (1<<7) != 0:
            flags.append("BFARVALID")
        if bfsr & (1<<4) != 0:
            flags.append("STKERR")
        if bfsr & (1<<3) != 0:
            flags.append("UNSTKERR")
        if bfsr & (1<<2) != 0:
            flags.append("IMPRECISERR")
        if bfsr & (1<<1) != 0:
            flags.append("PRECISERR")
        if bfsr & 1 != 0:
            flags.append("IBUSERR")
        
        return flags

    def extract_hfsr_flags(hfsr: int):
        flags = []

        if hfsr & (1<<30) != 0:
            flags.append("FORCED")
        if hfsr & (1<<1) != 0:
            flags.append("VECTTBL")
        
        return flags

    def read_BusFaultAddressRegister(self, auto_resume:bool = False) -> int:
        self.halt()

        b = self.read_memory_range(0xE000ED38, 4)
        add = int.from_bytes(b, byteorder='little')

        if auto_resume:
            self.resume()

        return add
        
    def read_MemManageFaultAddressRegister(self, auto_resume:bool = False) -> int:
        self.halt()

        b = self.read_memory_range(0xE000ED34, 4)
        add = int.from_bytes(b, byteorder='little')

        if auto_resume:
            self.resume()

        return add   


    def read_HardFaultStatusRegister(self, auto_resume:bool = False) -> int:
        self.halt()

        b = self.read_memory_range(0xE000ED2C, 4)
        add = int.from_bytes(b, byteorder='little')

        if auto_resume:
            self.resume()

        return add

    def set_soft_breakpoint(self, breakpoint_address:int,auto_resume:bool = False):
        self.halt()
        self.__send_str("bp " + hex(breakpoint_address) + " 2\n")       
        self.__read_until(b"> ") 

        if auto_resume == True:
            self.resume()

    def remove_soft_breakpoint(self, breakpoint_address:int,auto_resume:bool = False):
        self.halt()
        self.__send_str("rbp " + hex(breakpoint_address) + "\n")       
        self.__read_until(b"> ") 

        if auto_resume == True:
            self.resume()

    # Use Flash Patch feature
    def enable_hard_breakpoint(self, breakpoint_address:int, auto_resume:bool = False):
        FP_COMP0 = 0xE0002008
        # FP_REMAP = 0xE0002004
        FP_CTRL  = 0xE0002000

        if breakpoint_address & (7<<29) != 0:
            print("Hard breakpoints are only valid for addresses in flash memory")
        else:
            comp_address = breakpoint_address & 0x1FFFFFFC
            fpcr0 = 1 | comp_address | (0b11 << 30)
            self.write_memory_u32(FP_COMP0, fpcr0)

            fpcr = 0b11
            self.write_memory_u32(FP_CTRL, fpcr)

        if auto_resume:
            self.resume()

    def disable_hard_breakpoints(self, auto_resume:bool = False):
        FP_CTRL  = 0xE0002000
        fpcr = 0b10
        self.write_memory_u32(FP_CTRL, fpcr)

        if auto_resume:
            self.resume()

    def read_error_call_stack(self, auto_resume:bool = False):
        self.halt()

        regs = self.read_all_registers()
        lr = regs["lr"]
        sp = 0
        if lr & 0x4 != 0:
            #psp
            sp = regs["psp"]
        else:
            #msp
            sp = regs["msp"]

        call_stack = self.read_memory_range(sp, 8*4)
        names = ["r0", "r1", "r2", "r3", "r12", "lr", "pc", "xPSR"]
        call_dict = dict()
        for i in range(8):
            call_dict[names[i]] = int.from_bytes(call_stack[i*4:i*4+4], byteorder="little")

        if auto_resume:
            self.resume()

        return call_dict

        

    def __extract_reg_name(rstr: str) -> str:
        name_split = rstr.split(" ")
        if len(name_split) > 2:
            return name_split[1]
        else:
            return "unknown"

    def __extract_reg_value(rvstr: str) -> int:
        return int(rvstr.strip(), base=16)
