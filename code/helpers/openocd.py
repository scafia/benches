'''
File: openocd.py
Project: helpers
Created Date: Friday November 5th 2021
Author: Ronan (ronan.lashermes@inria.fr)
-----
Last Modified: Wednesday, 10th November 2021 4:50:05 pm
Modified By: Ronan (ronan.lashermes@inria.fr>)
-----
Copyright (c) 2021 INRIA
'''

import shlex, subprocess
import warnings
import socket
import time
from os.path import exists

class OpenOCD:

    def __init__(self, configuration_files, debug=False):
        self.config_files = configuration_files
        self.debug = debug

        # test files existance
        for cfile in self.config_files:
            if exists(cfile) == False:
                raise FileNotFoundError(cfile + ' does not exits')

        self.__launch_server()

    def __del__(self):
        if hasattr(self, 'ocd_server'):
            self.ocd_server.kill()


    def __launch_server(self):
        cmd = ['openocd']
        # set config files
        for cfile in self.config_files:
            cmd.append('-f')
            cmd.append(cfile)

        self.ocd_server = subprocess.Popen(cmd, stderr=subprocess.DEVNULL)
        # wait some time for the server to start. Trying to find something better.
        time.sleep(1)
        # by default, OpenOCD opens a server at localhost:4444
        
        self.session = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.session.settimeout(3.0)
        self.session.connect(('localhost', 4444))
        
        self.__read_until(b"> ")
    

    def __send_str(self, s:str):
        """Send a string to the OpenOCD server."""
        if isinstance(s, str):
            bs = s.encode("utf8")
            self.session.sendall(bs)

    def __read_until(self, p:bytes):
        """Read data from the OpenOCD server until the byte pattern is found."""
        b = bytearray()
        pattern_size = len(p)
        while True:
            b.extend(self.session.recv(1))
            last_pattern = b[-pattern_size:]
            if last_pattern == p:
                return b

    def __read_line(self):
        """Read one line from the OpenOCD server."""
        return self.__read_until(b"\n")

    def autonomous_reset(configuration_files):
        """Trigger an OpenOCD reset with a shell command, without using the OCD server."""

        for cfile in configuration_files:
            if exists(cfile) == False:
                raise FileNotFoundError(cfile + ' does not exits')

        cmd = 'openocd'
        # set config files
        for cfile in configuration_files:
            cmd += ' -f ' + cfile

        # reset
        cmd += ' -c "init\nreset run\n exit\n"'
        cmd_args = shlex.split(cmd)


        subprocess.run(cmd_args, capture_output=False, stderr=subprocess.DEVNULL)

    def reset(self):
        self.__send_str("reset run\n")
        self.__read_until(b"> ")
        self.halted = False

    def halt(self):
        self.__send_str("halt\n")
        self.__read_until(b"> ")
        self.halted = True

    def resume(self):
        self.__send_str("resume\n")
        self.__read_until(b"> ")
        self.halted = False

    def read_memory_range(self, base_address:int, byte_count:int):
        self.halt()
        cmd = "mdb " + hex(base_address) + " " + str(byte_count) + "\n"
        print(cmd)
        self.__send_str("mdb " + hex(base_address) + " " + str(byte_count) + "\n")
        r = self.__read_until(b"> ")
        print(r)

        self.resume()