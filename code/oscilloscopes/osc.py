'''
File: osc.py
Project: oscilloscopes
Created Date: Wednesday September 30th 2020
Author: Ronan (ronan.lashermes@inria.fr)
-----
Last Modified: Wednesday, 10th November 2021 9:16:24 am
Modified By: Ronan (ronan.lashermes@inria.fr>)
-----
Copyright (c) 2020 INRIA
'''

import warnings
from pint import Quantity, UnitRegistry

from helpers.ip_connection import IpConnection
from collections.abc import Sequence

def warning_on_one_line(message, category, filename, lineno, file=None, line=None):
        return '%s:%s:%s:\t%s\n' % (filename, lineno, category.__name__, message)

warnings.formatwarning = warning_on_one_line

class Oscilloscope:
    """An abstract oscilloscope."""
    
    def name(self) -> str:
        """Return the name of this oscilloscope."""
        return "Abstract oscilloscope"

    
    def __init__(self, connection:IpConnection=None, configuration:bytes=None):
        """
        Construct the oscilloscope object.
        
        Arguments:
            - connection: the Ip parameters to connect to the oscilloscope (address, port, ...)
            - configuration: a binary blob specifying the whole oscilloscope configuration.
        """
        self.ureg = UnitRegistry()
        if configuration is None:
            warnings.warn("The empty constructor must not be used for an experiment, please provide a starting configuration to ensure reproducibility.")
        else:
            self.load_configuration(configuration)

        connect_result = self.connect(connection)
        if connect_result == False:
            warnings.warn("Connection to {} failed".format(self.name()))

    def connect(self, connection:IpConnection) -> bool:
        """
        Initiate connection with the oscilloscope.
        Return true is connection is established.
        """
        raise NotImplementedError("Must override connect")


    # Configuration

    def reset_configuration(self):
        """Reset oscilloscope configuration."""
        pass

    def load_configuration(self, configuration:bytes):
        """Load a configuration into the oscilloscope from a binary blob."""
        pass

    def read_configuration(self) -> bytes:
        """Read whole configuration from oscilloscope into a binary blob."""
        pass

    def set_enabled_channels(self, channels: "Sequence[str]"):
        pass

    def get_enabled_channels(self) -> "Sequence[str]":
        pass

    def set_vertical_div(self, channel:str, div:Quantity):
        pass

    def get_vertical_div(self, channel:str) -> Quantity:
        pass

    def set_horizontal_div(self, div:Quantity):
        pass

    def get_horizontal_div(self) -> Quantity:
        pass

    def get_vertical_position(self, channel:str) -> Quantity:
        pass

    def set_vertical_position(self, channel:str, pos:Quantity):
        pass

    def get_horizontal_position(self) -> Quantity:
        pass

    def set_horizontal_position(self, pos:Quantity):
        pass

    # Acquisition

    def set_auto_sampling_rate(self):
        pass

    def get_sampling_rate(self) -> Quantity:
        pass

    def set_sampling_count(self, samples:int):
        """Set the number of samples in a trace."""
        pass

    def get_sampling_count(self) -> int:
        """Get the number of samples in a trace."""
        pass


    # Trace per trace mode

    def get_trace(self, channel:str) -> bytes:
        """Read the last measured trace from the oscilloscope."""
        pass

    def single(self):
        """Start a single measure: arm the trigger and wait for the signal."""
        pass

    def stop(self):
        """Stop the measures."""
        pass

    def run(self):
        """Start measuring."""
        pass

    def get_run_state(self) -> str:
        """
        Get the run state of the oscilloscope: "run", "stop", "single", "error"
        """
        pass


    # Segmented mode (record a lot of measures in one memory block on the oscilloscope)

    def set_segmented(self, segment_count: int) -> int:
        """
        Set the oscilloscope in segmented mode with given number of segment.
        Return selected number of segments.
        Assume one sample = 1 Byte.
        """
        pass

    def get_traces(self, channel:str):
        """Get all the traces in the oscilloscope memory."""
        pass

    
    # Trigger

    def set_trigger(self, channel:str, val:Quantity):
        """Set the trigger value and channel."""
        pass

    def set_trigger_auto(self):
        """Sets the trigger in automatic mode."""
        pass
