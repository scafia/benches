'''
File: rto2xxx.py
Project: oscilloscopes
Created Date: Wednesday September 30th 2020
Author: Ronan (ronan.lashermes@inria.fr)
-----
Last Modified: Wednesday, 16th December 2020 3:06:18 pm
Modified By: Ronan (ronan.lashermes@inria.fr>)
-----
Copyright (c) 2020 INRIA
'''

import warnings
from ..helpers.ip_connection import IpConnection
from .osc import *
import socket
import traceback
from pint import Quantity, UnitRegistry
from collections.abc import Sequence
from math import gcd
# import datetime
# from datetime import *

def gld(a: int, b: int) -> int:
    """
    Greatest lower divisor.
    Expect b < a.
    Return biggest c such that c | a and c < b.
    """

    d = gcd(a,b) # d is the step to decrement c
    c = (b // d) * d # biggest possible c value
    

    while d <= c: # we know that d | a and d < b, so d is a possible solution for c
        # does current c divides a ?
        g = gcd(c, a)
        if g == c:
            break

        # if not decrement c by step d
        c -= d

    return c

class Rto2xxx(Oscilloscope):

    available_channels = set(["CHAN1", "CHAN2", "CHAN3", "CHAN4"])
    wav_format = ""

    def name(self) -> str:
        return "RTO2XXX"

    def __init__(self, connection:IpConnection=None, configuration:bytes=None):
        Oscilloscope.__init__(self, connection, configuration)

        #get wave format
        self.wav_format = self.__command_str("FORM:DATA?\n")

        #disable interpolation
        self.__send_str(":ACQ:INT 0\n") # TO CHECK
        

    def connect(self, connection:IpConnection) -> bool:
        if connection is None:
            raise ValueError("Connection must not be None")

        # self.session = telnetlib.Telnet(connection.address, connection.port, connection.to)
        self.session = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.session.settimeout(1.0)
        self.session.connect((connection.address, connection.port))

        self.__send_str("*IDN?\n")
        id_str = self.__read_until(b"\n")

        return (b"RTO" in id_str)
        
    def test(self):
        self.__send_str("*IDN?\n")
        id_str = self.__read_until(b"\n")
        return id_str

    def __command_str(self, cmd:str) -> str:
        """Send a string to the oscilloscope and expect a string on one line as answer."""
        self.__send_str(cmd)
        return self.__read_line().strip()

    def __send_bytes(self, bs:bytes):
        """Send bytes to the oscilloscope."""
        self.session.sendall(bs)

    def __send_str(self, s:str):
        """Send a string to the oscilloscope."""
        if isinstance(s, str):
            bs = s.encode("utf8")
            self.session.sendall(bs)
        else:
            warnings.warn("{} is not a string: {}".format(s, type(s)))

    def __read_until(self, p:bytes):
        """Read data from the oscilloscope until the byte pattern is found."""
        b = bytearray()
        pattern_size = len(p)
        while True:
            b.extend(self.session.recv(1))
            last_pattern = b[-pattern_size:]
            if last_pattern == p:
                return b

    def __read_until_timeout(self):
        """Read data from the oscilloscope until no data is received in a given timespan."""
        b = bytearray()
        while True:
            try:
                new_buffer = self.session.recv(4096)
            except socket.error:
                return b
            else:
                b.extend(new_buffer)

    def __read_line(self):
        """Read one line from the oscilloscope."""
        return self.__read_until(b"\n")

    def __read_bytes(self, nb:int):
        """Read given number of bytes from the oscilloscope."""
        ba = bytearray()
        left = nb
        while left > 0:
            new_bytes = self.session.recv(left)
            left -= len(new_bytes)
            ba.extend(new_bytes)
        return bytes(ba)

    def __read_header_byte_count(self):
        """Read a response header of the form #XYYYYY where X is the number of Y digits"""
        first_char = self.__read_bytes(1)

        while first_char == b"\n":
            first_char = self.__read_bytes(1)

        if first_char != b"#":
            raise Exception("Expecting to read #, got " + str(first_char) + " instead.")

        nb_digits = int(self.__read_bytes(1).decode("utf-8"))
        count = int(self.__read_bytes(nb_digits).decode("utf-8"))
        return count

    def reset_configuration(self):
        """Reset oscilloscope configuration."""
        self.__send_str("*RST\n")

    def read_configuration(self) -> bytes:
        """Read whole configuration from oscilloscope into a binary blob."""
        self.__send_str("MMEM:CDIR 'C:\\saves'\n")
        self.__send_str("MMEM:SAV 'C:\\saves\\temp.bin'\n")
        
        self.__send_str("MMEM:DATA? 'temp.bin'\n")
        # raise Exception("Configuration management not supported yet.")

        ans = self.__read_until_timeout()
        return ans

        # self.__send_str("*LRN?\n")
        # ans = self.__read_until(b"</setup>\r\n\n")
        # return ans

    # """
    # Load a configuration from former read.
    # """
    def load_configuration(self, configuration:bytes):
        self.__send_str("MMEM:CDIR 'C:\\saves'\n")
        self.__send_str("MMEM:DATA 'temp.bin', ")
        self.__send_bytes(configuration)

        self.__send_str("MMEM:LOAD:STAT 2, 'C:\\saves\\temp.bin'\n")
        self.__send_str("*RCL 2\n")
        # raise Exception("Configuration management not supported yet.")
        # self.__send_bytes(configuration)

    def __is_valid_channel(self, channel:str) -> bool:
        if channel in self.available_channels:
            return True
        else:
            warnings.warn("{} is not a valid channel.".format(channel))
            return False


    def set_enabled_channels(self, channels: "Sequence[str]"):
        # disable all channels
        for c in self.available_channels:
            self.__send_str(":" + c + ":DISP 0\n")
        
        # reenable selected channels
        for c in channels:
            if self.__is_valid_channel(c):
                self.__send_str(":" + c + ":DISP 1\n")

    def get_enabled_channels(self):
        result = []
        for c in self.available_channels:
            self.__send_str(":" + c + ":DISP?\n")
            ans = self.__read_line().strip()
            if ans == b"1":
                result.append(c)

        return result

    def set_vertical_div(self, channel:str, div:Quantity):
        if self.__is_valid_channel(channel):
            divV = div.to(self.ureg.volt)
            self.__send_str(":" + channel + ":SCAL " + "{:e}".format(divV.magnitude) + "\n")

    def get_vertical_div(self, channel:str) -> Quantity:
        if self.__is_valid_channel(channel):
            self.__send_str(":" + channel + ":SCAL?\n")
            ans = self.__read_line().strip()
            fans = float(ans.decode("utf-8"))
            return Quantity(fans, self.ureg.volt).to_compact()
        raise Exception("Invalid channel")

    def set_horizontal_div(self, div:Quantity):
        divS = div.to(self.ureg.second)
        self.__send_str(":TIM:SCAL " + "{:e}".format(divS.magnitude) + "\n")

    def get_horizontal_div(self):
        self.__send_str(":TIM:SCAL?\n")
        ans = self.__read_line().strip()
        fans = float(ans.decode("utf-8"))
        return Quantity(fans, self.ureg.second).to_compact()


    def get_vertical_position(self, channel:str) -> Quantity:
        if self.__is_valid_channel(channel):
            self.__send_str(":" + channel + ":OFFS?\n")
            ans = self.__read_line().strip()
            fans = float(ans.decode("utf-8"))
            return Quantity(fans, self.ureg.volt).to_compact()
        raise Exception("Invalid channel")
        

    def set_vertical_position(self, channel:str, pos:Quantity):
        if self.__is_valid_channel(channel):
            posV = pos.to(self.ureg.volt)
            self.__send_str(":" + channel + ":OFFS " + "{:e}".format(posV.magnitude) + "\n")

    def get_horizontal_position(self) -> Quantity:
        self.__send_str(":TIM:POS?\n")
        ans = self.__read_line().strip()
        fans = float(ans.decode("utf-8"))
        return Quantity(fans, self.ureg.second).to_compact()

    def set_horizontal_position(self, pos:Quantity):
        posS = pos.to(self.ureg.second)
        self.__send_str(":TIM:POS " + "{:e}".format(posS.magnitude) + "\n")


    # Aquisition

    def get_sampling_rate(self) -> Quantity:
        self.__send_str(":ACQ:SRAT?\n")
        ans = self.__read_line().strip()
        fans = float(ans.decode("utf-8"))
        return Quantity(fans, self.ureg.hertz).to_compact()

    def set_sampling_count(self, samples:int):
        self.__send_str(":ACQ:POIN " + str(samples) + "\n")

    def get_sampling_count(self) -> int:
        self.__send_str(":ACQ:POIN?\n")
        ans = self.__read_line().strip()
        ians = int(ans.decode("utf-8"))
        return ians

    def set_auto_sampling_rate(self):
        self.__send_str(":ACQ:SRAT:AUTO 1\n")
        

    # Trace per trace mode

    def set_realtime(self):
        """Set the oscilloscope in real time mode."""
        self.__send_str(":ACQ:MODE RTIM\n") #

    def get_trace(self, channel:str) -> bytes:
        """Read the last measured trace from the oscilloscope."""
        if self.__is_valid_channel(channel):
            if self.wav_format != "INT,8":
                self.__send_str("FORM:DATA INT,8\n")
                self.wav_format = "INT,8"

            #self.__send_str(":WAV:DATA?\n")
            self.__send_str(channel + ":DATA?\n")
            #CHANnel<m>[:WAVeform<n>]:DATA[:VALues]?
            trace_size = self.__read_header_byte_count()
            # print("Trace with %i elements" %trace_size)
            trace = self.__read_bytes(trace_size)
            return trace

        raise Exception("Invalid channel")


    def single(self):
        self.__send_str(":SINGLE\n")

    def stop(self):
        self.__send_str(":STOP\n")
        while self.get_run_state() != "stop":
            continue

    def run(self):
        self.__send_str(":RUN\n")

    def get_run_state(self) -> str:
        ans = self.__command_str(":RST?\n")
        switch = {
            "SING": "single",
            "RUN": "run",
            "STOP": "stop"
        }

        return switch.get(ans.decode("utf-8"), "Error")

    # Segmented mode (record a lot of measures in one memory block on the oscilloscope)

    def __get_max_memory_depth(self):
        """Get the maximum memory depth."""
        self.__send_str(":ACQ:POIN:TESTLIMITS?\n")
        ans = self.__read_line().strip()
        semicolumn = ans.index(b":")
        max_depth = int(ans[semicolumn + 1:])
        return max_depth

    


    def set_segmented(self, max_segment_count: int) -> int:
        # expected samples per trace
        trace_size = self.get_sampling_count()

        # memory limits
        max_depth = self.__get_max_memory_depth()

        max_traces = max_depth // trace_size
        print("Max traces: " + str(max_traces))
        selected_seg_count = max_segment_count

        # if not enough memory for all segments, use gcd.
        if max_traces < max_segment_count:
            selected_seg_count = gld(max_segment_count, max_traces)

        self.__send_str(":ACQ:MODE SEGM\n")
        self.__send_str(":WAV:SEGM:ALL 1\n")
        self.__send_str(":ACQ:SEGM:COUN {}\n".format(selected_seg_count))
        return selected_seg_count



    def get_traces(self, channel:str):
        """Get all the traces in the oscilloscope memory."""
        pass

    
    # Trigger

    def set_trigger(self, channel:str, val:Quantity):
        """Set the trigger value and channel."""
        
        if self.__is_valid_channel(channel):
            valV = val.to(self.ureg.volt)
            self.__send_str(":TRIG:MODE EDGE\n")
            self.__send_srt(":TRIG:SWE TRIG\n")
            self.__send_str(":TRIG:EDGE:SLOP POS\n")
            self.__send_str(":TRIG:LEV {},{}\n".format(channel, valV))

    def set_trigger_auto(self):
        """Sets the trigger in automatic mode."""
        self.set_realtime()
        self.__send_srt(":TRIG:SWE AUTO\n")
