

import warnings
from ..helpers.ip_connection import IpConnection
from .gen import *
import socket
import traceback
from pint import Quantity, UnitRegistry
# from collections.abc import Sequence

class K33509B(Generator):

    

    def name(self) -> str:
        return "33509B"

    def __init__(self, connection:IpConnection=None, configuration:bytes=None):
        Generator.__init__(self, connection, configuration)
        

    def connect(self, connection:IpConnection) -> bool:
        if connection is None:
            raise ValueError("Connection must not be None")

        self.session = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.session.settimeout(3.0)
        self.session.connect((connection.address, connection.port))


        self.send_str("*IDN?\n")
        id_str = self.read_until(b"\n")

        return (b"33509B" in id_str)

    def write_bytes(self, bs:bytes):
        """Send bytes to the oscilloscope."""
        self.session.sendall(bs)

    def try_read_bytes(self, nb:int) -> int:
        """Try to read the given number of bytes."""
        return self.session.recv(nb)

    def reset_configuration(self):
        """Reset oscilloscope configuration."""
        self.send_str("*RST\n")

    def read_configuration(self) -> bytes:
        """Read whole configuration from oscilloscope into a binary blob."""
        raise NotImplementedError("")
        # self.__send_str("*LRN?\n")
        # ans = self.__read_until(b"</setup>\r\n\n")
        # return ans

    # """
    # Load a configuration from former read.
    # """
    def load_configuration(self, configuration:bytes):
        # self.__send_bytes(configuration)
        raise NotImplementedError("")

    def set_trigger_delay(self, delay:Quantity):
        """Set the delay between the trigger and the signal generation."""
        delS = delay.to(self.ureg.second)
        self.send_str("TRIG:DEL " + "{:e}".format(delS.magnitude) + "\n")

    def get_trigger_delay(self) -> Quantity:
        """Get the delay between the trigger and the signal generation."""
        ans = self.command_str("TRIG:DEL?\n")
        fans = float(ans.decode("utf-8"))
        return Quantity(fans, self.ureg.second).to_compact()

