

import warnings
from ..helpers.ip_connection import IpConnection
from .gen import *
import socket
import traceback
from pint import Quantity, UnitRegistry
# from collections.abc import Sequence

class K81160A(Generator):

    

    def name(self) -> str:
        return "81160A"

    def __init__(self, connection:IpConnection=None, configuration:bytes=None):
        Generator.__init__(self, connection, configuration)
        

    def connect(self, connection:IpConnection) -> bool:
        if connection is None:
            raise ValueError("Connection must not be None")

        self.session = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.session.settimeout(6.0)
        self.session.connect((connection.address, connection.port))


        self.send_str("*IDN?\n")
        id_str = self.read_until(b"\n")

        return (b"81160A" in id_str)

    def write_bytes(self, bs:bytes):
        """Send bytes to the oscilloscope."""
        self.session.sendall(bs)

    def try_read_bytes(self, nb:int) -> int:
        """Try to read the given number of bytes."""
        return self.session.recv(nb)

    def reset_configuration(self):
        """Reset oscilloscope configuration."""
        self.send_str("*RST\n")

    def read_configuration(self) -> bytes:
        """Read whole configuration from oscilloscope into a binary blob."""
        raise NotImplementedError("")
        # self.__send_str("*LRN?\n")
        # ans = self.__read_until(b"</setup>\r\n\n")
        # return ans

    # """
    # Load a configuration from former read.
    # """
    def load_configuration(self, configuration:bytes):
        # self.__send_bytes(configuration)
        raise NotImplementedError("")

    def set_trigger_delay(self, delay:Quantity):
        """Set the delay between the trigger and the signal generation."""
        delS = delay.to(self.ureg.second)
        self.send_str("PULS:DEL " + "{:e}".format(delS.magnitude) + "\n")

    def get_trigger_delay(self) -> Quantity:
        """Get the delay between the trigger and the signal generation."""
        ans = self.command_str(":PULS:DEL?\n")
        fans = float(ans.decode("utf-8"))
        return Quantity(fans, self.ureg.second).to_compact()

    def set_burst_count(self, burst_count:int):
        self.send_str(":TRIG:COUN " + str(burst_count) + "\n")

    def get_burst_count(self) -> int:
        ans = self.command_str(":TRIG:COUN?\n")
        return int(ans)

    def set_voltage_amplitude(self, vamp:Quantity):
        vampdBm = vamp.to(self.ureg.DBM)
        self.send_str(":VOLT " + "{:e}".format(vampdBm.magnitude) + " DBM\n")
        
    def set_output_enable(self, enable:bool):
        if enable:
            self.send_str(":OUTP ON\n")
        else:
            self.send_str(":OUTP OFF\n")

    def set_frequency(self, freq:Quantity):
        freqHz = freq.to(self.ureg.hertz)
        self.send_str(":FREQ " + "{e}".format(freqHz.magnitude) + "\n")