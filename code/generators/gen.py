

import warnings
from pint import Quantity, UnitRegistry

from helpers.ip_connection import IpConnection
# from collections.abc import Sequence

def warning_on_one_line(message, category, filename, lineno, file=None, line=None):
        return '%s:%s:%s:\t%s\n' % (filename, lineno, category.__name__, message)

warnings.formatwarning = warning_on_one_line

class Generator:
    """An abstract signal generator."""
    
    def name(self) -> str:
        """Return the name of this signal generator."""
        return "Abstract signal generator"

    
    def __init__(self, connection:IpConnection=None, configuration:bytes=None):
        """
        Construct the signal generator object.
        
        Arguments:
            - connection: the Ip parameters to connect to the signal generator (address, port, ...)
            - configuration: a binary blob specifying the whole signal generator configuration.
        """
        self.ureg = UnitRegistry()
        if configuration is None:
            warnings.warn("The empty constructor must not be used for an experiment, please provide a starting configuration to ensure reproducibility.")
        else:
            self.load_configuration(configuration)

        connect_result = self.connect(connection)
        if connect_result == False:
            warnings.warn("Connection to {} failed".format(self.name()))

    def connect(self, connection:IpConnection) -> bool:
        """
        Initiate connection with the signal generator.
        Return true is connection is established.
        """
        raise NotImplementedError("Must override connect")


    # Configuration

    def reset_configuration(self):
        """Reset signal generator configuration."""
        pass

    def load_configuration(self, configuration:bytes):
        """Load a configuration into the signal generator from a binary blob."""
        pass

    def read_configuration(self) -> bytes:
        """Read whole configuration from signal generator into a binary blob."""
        pass

    # Communications

    def write_bytes(self, bs: bytes):
        """Write bytes to pipe. All bytes must be sent or an exception raised."""
        pass

    def try_read_bytes(self, nb: int) -> bytes:
        """Try to read a number of bytes."""
        pass

    def command_str(self, cmd:str) -> str:
        """Send a string to the oscilloscope and expect a string on one line as answer."""
        self.send_str(cmd)
        return self.read_line().strip()

    def send_str(self, s:str):
        """Send a string to the oscilloscope."""
        if isinstance(s, str):
            bs = s.encode("utf8")
            self.write_bytes(bs)
        else:
            warnings.warn("{} is not a string: {}".format(s, type(s)))

    def read_until(self, p:bytes):
        """Read data from the oscilloscope until the byte pattern is found."""
        b = bytearray()
        pattern_size = len(p)
        while True:
            b.extend(self.try_read_bytes(1))
            last_pattern = b[-pattern_size:]
            if last_pattern == p:
                return b

    def read_line(self):
        """Read one line from the oscilloscope."""
        return self.read_until(b"\n")

    def read_bytes(self, nb:int):
        """Read given number of bytes from the oscilloscope."""
        ba = bytearray()
        left = nb
        while left > 0:
            new_bytes = self.try_read_bytes(left)
            left -= len(new_bytes)
            ba.extend(new_bytes)
        return bytes(ba)

    def set_trigger_delay(self, delay:Quantity):
        """Set the delay between the trigger and the signal generation."""
        pass

    def get_trigger_delay(self) -> Quantity:
        """Get the delay between the trigger and the signal generation."""
        pass