'''
File: K33509B copy.py
Project: generators
Created Date: Friday September 2nd 2022
Author: Ronan (ronan.lashermes@inria.fr)
-----
Last Modified: Friday, 2nd September 2022 1:34:50 pm
Modified By: Ronan (ronan.lashermes@inria.fr>)
-----
Copyright (c) 2022 INRIA
'''


import warnings
from helpers.ip_connection import IpConnection
from generators.gen import *
import socket
import traceback
from telnetlib import Telnet
from pint import Quantity, UnitRegistry
from time import sleep
# from collections.abc import Sequence

class AVRK4(Generator):

    

    def name(self) -> str:
        return "ARVK4"

    def __init__(self, connection:IpConnection=IpConnection("192.168.0.101", 23), configuration:bytes=None):
        Generator.__init__(self, connection, configuration)
        

    def connect(self, connection:IpConnection) -> bool:
        if connection is None:
            raise ValueError("Connection must not be None")

        self.session = Telnet(connection.address, connection.port, 1.0)
        # self.session = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        # self.session.settimeout(5.0)
        # self.session.connect((connection.address, connection.port))

        # login / pass

        # login / pass

        self.read_until(b": ")
        self.send_str("admin\n")
        self.read_until(b": ")
        self.send_str("default\n")
        self.read_until(b"> ")
        
        self.send_str("*IDN?\n")
        self.read_until(b"*IDN?\r\n") # discard echo
        id_str = self.read_until(b"\n")
        self.read_until(b"> ")

        return (b"AVRK-4" in id_str)

    def read_until(self, p: bytes):
        return self.session.read_until(p)

    def write_bytes(self, bs: bytes):
        """Send bytes to the oscilloscope."""
        return self.session.write(bs)

    def send_str(self, s:str):
        """Send a string to the oscilloscope."""
        if isinstance(s, str):
            bs = s.encode("ascii")
            self.write_bytes(bs)
        else:
            warnings.warn("{} is not a string: {}".format(s, type(s)))

    def close(self):
        self.session.close()

    def try_read_bytes(self, nb:int) -> bytes:
        """Try to read the given number of bytes."""
        rx = 0
        bytes_arr = bytearray()
        while rx < nb:
            newchar = self.session.read_some()
            if newchar == b'': #EOF
                return bytes_arr
            else:
                bytes_arr.append(newchar)


        return bytes_arr

    def reset_configuration(self):
        """Reset oscilloscope configuration."""
        self.send_str("*RST\n")

    def read_configuration(self) -> bytes:
        """Read whole configuration from oscilloscope into a binary blob."""
        raise NotImplementedError("")
        # self.__send_str("*LRN?\n")
        # ans = self.__read_until(b"</setup>\r\n\n")
        # return ans

    # """
    # Load a configuration from former read.
    # """
    def load_configuration(self, configuration:bytes):
        # self.__send_bytes(configuration)
        raise NotImplementedError("")

    def set_ext_trig(self):
        self.send_str("TRIG:SOUR EXT\n") # external source

    def set_trigger_delay(self, delay:Quantity):
        """Set the delay between the trigger and the signal generation."""
        self.send_str("TRIG:SOUR EXT\n") # external source
        delS = delay.to(self.ureg.second)
        self.send_str("puls:DEL " + "{:e}".format(delS.magnitude) + "\n")

    def set_width(self, width:Quantity):
        """Set the width of the signal."""
        #self.send_str("TRIG:SOUR EXT\n") # external source
        widthS = width.to(self.ureg.second)
        self.send_str("puls:WIDT " + "{:e}".format(widthS.magnitude) + "\n")

    def get_trigger_delay(self) -> Quantity:
        """Get the delay between the trigger and the signal generation."""
        ans = self.command_str("puls:DEL?\n")
        fans = float(ans.decode("utf-8"))
        return Quantity(fans, self.ureg.second).to_compact()

    def activate(self):
        if not self.isActive():
            self.send_str("OUTPUT on\n")
            while not self.isActive():
                sleep(0.5)
        

    def shutdown(self):
        if self.isActive():
            self.send_str("OUTPUT off\n")
            while self.isActive():
                sleep(0.5)

    def isActive(self):  
        self.send_str("OUTPUT?\n")
        self.read_until(b"OUTPUT?\r\n")
        dataread = self.read_until(b"> ")
        act = (dataread[0] == b'1'[0])
        return act

    def set_amplitude(self, voltage:Quantity):
        vv = voltage.to(self.ureg.volt)
        self.send_str("volt " + "{:e}".format(vv.magnitude) + "\n")

