
from stages.xyz_stage import XYZStage
from pint import Quantity, UnitRegistry
import curses
from time import sleep
import traceback
from os import listdir

def find_port():
    ttyUSBX = ["/dev/"+f for f in listdir("/dev") if "ttyUSB" in f]

    for ttyUSB in ttyUSBX:
            try:
                stage = XYZStage(ttyUSB)
                if stage.test() == True:
                    return ttyUSB
            except:
                continue

    return None

def main(win):
    annoted_points = []
    ureg = UnitRegistry()
    step = Quantity(1, ureg.mm)
    last_move = ""
    zup = False

    curses.start_color()
    curses.echo()
    curses.use_default_colors()
    for i in range(0, curses.COLORS):
        curses.init_pair(i + 1, i, -1)

    stage_tty = find_port()
    # stage_tty = "/dev/ttyUSB0"
    print("Stage port is" + stage_tty)
    stage = XYZStage(stage_tty)

    if stage.needHoming() == True:
        win.clear()
        l = 0
        if stage.x.needHoming() == True:
            win.addstr("X needs homing.\n\r")
            l += 1
        if stage.y.needHoming() == True:
            win.addstr("Y needs homing.\n\r")
            l += 1
        if stage.z.needHoming() == True:
            win.addstr("Z needs homing.\n\r")
            l += 1
        win.addstr("Stage is about to perform HOMING operation: make sure that enough space is available.\n\rConfim (yes/*) ? ")
        l += 2

        conf_str = win.getstr(l,0, 3)

        if conf_str == b"yes":
            stage.homing(True)
        else:
            win.addstr("Homing cancelled. Exiting.")
            return


    curses.noecho()

    def draw(win):
        xPos = stage.x.getPos()
        yPos = stage.y.getPos()
        zPos = stage.z.getPos()

        win.clear()
        win.addstr("Move the stage with keypad. Exit with ESCAPE.\n")
        win.addstr("Current position is: \n\tX = " + str(xPos) + "\n\tY = " + str(yPos) + "\n\tZ = " + str(zPos) + "\n")
        win.addstr("Step:\n\t" + str(step) + "\n")
        win.addstr("Last move:\n\t" + last_move + "\n")
    
        win.addstr(str(len(annoted_points)) + " annotated points:\n")
        for (aX, aY, aZ) in annoted_points:
            win.addstr("\t(X=" + str(aX) + ", Y=" + str(aY) + ", Z=" + str(aZ) + ")\n", curses.color_pair(3))

    def pointsToPlane():
        pass


    #win.nodelay(True)
    key=""
    draw(win)
    while 1:          
        try:
            curses.flushinp()            
            key = win.getch()    

            if key != -1:     # in case of nodelay==True                

                if key == 27:
                    break        

                xPos = stage.x.getPos()
                yPos = stage.y.getPos()
                zPos = stage.z.getPos()

                if key == 55: #7
                    step = step*2

                if key == 49: #1
                    step = step/2

                if key == 56: #8
                    last_move = "Y-" + str(step)
                    #stage.y.move(yPos - step)
                    stage.moveXY(xPos, yPos - step, zup)
                if key == 50: #2
                    last_move = "Y+" + str(step)
                    #stage.y.move(yPos + step)
                    stage.moveXY(xPos, yPos+step, zup)

                if key == 52: #4
                    last_move = "X-" + str(step)
                    #stage.x.move(xPos - step)
                    stage.moveXY(xPos-step, yPos, zup)
                if key == 54: #6
                    last_move = "X+" + str(step)
                    #stage.x.move(xPos + step)
                    stage.moveXY(xPos+step, yPos, zup)

                if key == 57: #9
                    last_move = "Z-" + str(step)
                    stage.z.move(zPos - step)
                if key == 51: #3
                    last_move = "Z+" + str(step)
                    stage.z.move(zPos + step)

                if key == 48: #0
                    try:
                        area = stage.computeAreaOfInterest(annoted_points)
                        area.save("area")
                        win.addstr("Area saved to disk.")
                        sleep(1)
                    except Exception as e:
                        win.addstr("Error" + str(e) + "\n\r")
                        win.addstr(traceback.format_exc())
                        win.addstr("\n\rPress a key to acknowledge.")
                        win.getch()
                    #stage.zero()

                if key == 53: #5, register pos
                    annoted_points.append((xPos, yPos, zPos))

                #if key == 10: #keypad enter
                #    stage.zero()


                stage.waitForMoveStop()
                draw(win)
                #win.addstr(str(key)) 
        except Exception as e:
           # No input   
           pass         

curses.wrapper(main)