with import <nixpkgs> {};

let
     pkgs = import (builtins.fetchGit {
         # Descriptive name to make the store path easier to identify                
         name = "my-old-revision";                                                 
         url = "https://github.com/nixos/nixpkgs-channels/";                       
         ref = "refs/heads/nixpkgs-20.03-darwin";                     
         rev = "1975b8687474764c157e6a220fdcad2c5dc348a1";                                           
     }) {};                                                                           
in

let
  pythonEnv = pkgs.python38.withPackages (ps: [
    ps.setuptools
    ps.pint
    ps.numpy
    ps.pyserial
    ps.termcolor
    ps.shapely
    ps.jsonpickle
  ]);
in

 # Make a new "derivation" that represents our shell
stdenv.mkDerivation {
  name = "dev-env";

  # The packages in the `buildInputs` list will be added to the PATH in our shell
  buildInputs = with pkgs; [
    pythonEnv
    bash
  ];
}
